import { createStore, applyMiddleware, compose } from 'redux';
import { Reducers } from './reducers/reducers';
import createSagaMiddleware from "redux-saga";
import { watcherSaga } from "../saga/root-saga";

// create the saga middleware
const sagaMiddleware = createSagaMiddleware();

// dev tools middleware
const reduxDevTools =
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();

export const Store = createStore(
    Reducers,
    applyMiddleware(sagaMiddleware)
);

// run the saga
sagaMiddleware.run(watcherSaga);