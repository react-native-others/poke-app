import { GET_ALL, GET_POKEMON_DETAILS_REQUEST, API_CALL_REQUEST } from '../actions/pokemon-action-types';

export const getAll = value => ({
    type: API_CALL_REQUEST,
    payload: {
        id: 1
    }
});

export const getDetails = value => ({
    type: GET_POKEMON_DETAILS_REQUEST,
    payload: {
        id: 10
    }
});