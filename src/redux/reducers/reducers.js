import { pokemonReducer } from './pokemon-reducer';
import { combineReducers } from 'redux';
import { pokemonDetailsReducer } from './pokemon-details-reducer';

export const Reducers = combineReducers({
  pokemonState: pokemonReducer,
  pokemonDetailsState: pokemonDetailsReducer
});