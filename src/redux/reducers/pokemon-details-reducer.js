import { 
    GET_ALL,
    GET_POKEMON_DETAILS_ERROR,
    GET_POKEMON_DETAILS_REQUEST,
    GET_POKEMON_DETAILS_SUCCESS
} from '../actions/pokemon-action-types';

const initialState = {
    fetching: false,
    pokemon: {},
    error: null
};
export const pokemonDetailsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_POKEMON_DETAILS_REQUEST:
            return { ...state, fetching: true, error: null };
        case GET_POKEMON_DETAILS_SUCCESS:
            return { ...state, fetching: false, pokemon: action.pokemon };
        case GET_POKEMON_DETAILS_ERROR:
            return { ...state, fetching: false, error: action.error };
        default:
            return state;
    }
};