import { 
    GET_ALL,
    API_CALL_FAILURE,
    API_CALL_REQUEST,
    API_CALL_SUCCESS,
    API_CALL_TYPES_REQUEST
} from '../actions/pokemon-action-types';

const initialState = {
    fetching: false,
    pokemons: [],
    error: null,
    sections: [
        {
            title: 'Flying',
            name: 'flying',
            pokemons: []
        },
        {
            title: 'Fire',
            name: 'fire',
            pokemons: []
        },
        {
            title: 'Psychic',
            name: 'psychic',
            pokemons: []
        },
        {
            title: 'Water',
            name: 'water',
            pokemons: []
        }
    ]
};
export const pokemonReducer = (state = initialState, action) => {
    ;
    switch (action.type) {
        case API_CALL_TYPES_REQUEST:
            return { ...state, fetching: true, error: null };
        case API_CALL_REQUEST:
            return { ...state, fetching: true, error: null };
        case API_CALL_SUCCESS:
            switch (action.call_type) {
                case 'sections':
                    return { ...state, fetching: false, sections: action.sections };
                case 'pokemons':
                    return { ...state, fetching: false, pokemons: action.pokemons };
                default:
                    ;
                    return { ...state, fetching: false };
            }
        case API_CALL_FAILURE:
            return { ...state, fetching: false, error: action.error };
        default:
            return state;
    }
};