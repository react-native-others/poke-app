import React, { Component } from 'react';
import { Platform, Button, StyleSheet, Text, View, SafeAreaView, ScrollView } from 'react-native';
import TypeList from '../../shared/components/TypeList/TypeList';
import AppSection from '../../shared/components/AppSection/AppSection';
import { connect } from 'react-redux';
import { API_CALL_REQUEST } from '../../redux/actions/pokemon-action-types';
import BigCarg from '../../shared/components/BigCard/BigCard';
import { FlatList } from 'react-native-gesture-handler';

class FavoritesPage extends Component {
    static navigationOptions = {
        title: 'Favorites',
    };

    componentDidMount() {
        
    }

    render() {
        const pokemons = [{}, {}, {}]
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView>

                <FlatList 
                    style={{marginTop: 20}}
                    data={ pokemons }
                    renderItem={({item}) => 
                        <BigCarg />
                    }
                    keyExtractor={((item, index) => index.toString())}
                />

                </ScrollView>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (store) => ({
    pokemons: store.pokemonState.pokemons,
    fetching: store.pokemonState.fetching,
});

const mapDispatchToProps = dispatch => {
return {
    // dispatching plain actions
    onRequestPokemons: () => dispatch({ type: API_CALL_REQUEST }),
}
}

export default connect(mapStateToProps, mapDispatchToProps)(FavoritesPage);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
