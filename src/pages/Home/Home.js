import React, { Component } from 'react';
import { Platform, Button, StyleSheet, Text, View, SafeAreaView, ScrollView, FlatList } from 'react-native';
import TypeList from '../../shared/components/TypeList/TypeList';
import AppSection from '../../shared/components/AppSection/AppSection';
import { connect } from 'react-redux';
import { firebaseService } from '../../../Firebase';
import { getAll } from '../../redux/actions/pokemon-actions';
import BigCarg from '../../shared/components/BigCard/BigCard';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { authService } from '../../shared/services/AuthService';

class HomePage extends Component {
    static navigationOptions = {
        title: 'Home',
    };

    componentWillMount() {
        authService.login('charles-franca@live.com', 'ERfdsx!1544758');
        // firebaseService.createDoc();
    }

    componentDidMount() {
        this.props.onRequestPokemonTypes(this.props.sections);
        this.props.onRequestPokemons();
    }

    renderSections(sections) {
        return sections.map(s => {
            return (
                <AppSection 
                    title={s.title}
                    key={s.name}>
                    <TypeList 
                        pokemons={s.pokemons}
                        onItemClicked={(item) => {
                            this.props.navigation.navigate('Details', {pokemon: item});
                        }}
                    />
                </AppSection>
            )
        })
    }

    render() {
        const { pokemons, sections } = this.props;
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={{width: '100%'}}>

                    {this.renderSections(sections)}

                    <AppSection 
                        title={'Others'}>
                        <FlatList 
                            style={{marginTop: 20}}
                            data={ pokemons }
                            renderItem={({item}) => 
                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.navigation.navigate('Details', {
                                            pokemon: item
                                        });
                                    }}>
                                    <BigCarg 
                                        pokemon={item}
                                    />
                                </TouchableOpacity>
                            }
                            keyExtractor={((item, index) => index.toString())}
                        />
                    </AppSection>

                </ScrollView>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (store) => ({
    pokemons: store.pokemonState.pokemons,
    fetching: store.pokemonState.fetching,
    sections: store.pokemonState.sections,
});

const mapDispatchToProps = dispatch => {
    return {
        // dispatching plain actions
        onRequestPokemonTypes: (sections) => dispatch({
            type: 'API_CALL_TYPES_REQUEST',
            payload: {
                sections: sections
            }
        }),
        onRequestPokemons: () => dispatch({
            type: 'API_CALL_POKEMONS_REQUEST',
            payload: {
            }
        }),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
