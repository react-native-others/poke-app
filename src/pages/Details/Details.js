import React, { Component } from 'react';
import { Platform, Button, StyleSheet, Text, View, SafeAreaView, ScrollView, Image } from 'react-native';
import TypeList from '../../shared/components/TypeList/TypeList';
import AppSection from '../../shared/components/AppSection/AppSection';
import { connect } from 'react-redux';
import { GET_POKEMON_DETAILS_REQUEST } from '../../redux/actions/pokemon-action-types';
import BigCarg from '../../shared/components/BigCard/BigCard';
import ImagePicker from 'react-native-image-picker';

// More info on all the options is below in the API Reference... just some common use cases shown here
const options = {
    title: 'Select Image',
    // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};

class DetailsPage extends Component {
    static navigationOptions = (nav) => {
        
        const pokemon = nav.navigation.getParam('pokemon', {});
        return {
            title: pokemon.name,
        }
    };

    state = {
        detailsImage: ''
    }

    componentDidMount() {
        const { navigation } = this.props;
        const pokemon = navigation.getParam('pokemon', {});
        this.props.onRequestPokemons(pokemon.name);
    }

    chooseImage() {
        ImagePicker.showImagePicker(options, (response) => {
            ;
            if (response.didCancel) {
                ;
            } else if (response.error) {
                ;
            } else if (response.customButton) {
                ;
            } else {
                const source = response.uri;
                this.setState({detailsImage: source})
                ;
            }
        });
    }

    renderPokemonTypes(types) {
        if (!types) {
            return null;
        }
        return (
            types.map(t => {
                return (
                    <Text key={t}>{t}</Text>
                )
            })
        )
    }

    renderEvolutions(evolutions) {
        if (!evolutions) {
            return null;
        }
        return (
            evolutions.map(e => {
                return (
                    <View style={{
                        width: 113,
                        height: 113,
                        flex: 1,
                        alignContent: 'center',
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderWidth: 2,
                        borderRadius: 113,
                        borderColor: '#D83939',
                        overflow: "hidden",
                        flexDirection: 'row',
                        marginBottom: 20
                    }}>
                        <Image
                            key={e.name}
                            source={{
                                uri: e.image
                            }}
                            style={{ width: 90, height: 90 }}
                            resizeMode={'contain'}
                        />
                    </View>
                )
            })
        )
    }

    render() {
        const { navigation } = this.props;
        let pokemon = navigation.getParam('pokemon', {});
        ;
        pokemon = this.props.pokemon;
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView>
                    <View
                        style={{
                            
                        }}>
                        <Button 
                            title={'Choose Image'}
                            onPress={() => {
                                this.chooseImage();
                            }}
                        />
                        {
                            this.state.detailsImage ? <Image 
                                source={{
                                    uri: this.state.detailsImage
                                }}
                                style={{
                                    width: 100,
                                    height: 100
                                }}
                            /> : null
                        }
                    </View>
                    <BigCarg
                        pokemon={pokemon}
                    />

                    <View>
                        <Text>{pokemon.description}</Text>
                    </View>

                    <AppSection
                        title={'Caracteristicas'}>

                        <Text>
                            Peso: {pokemon.weight}
                        </Text>
                        <Text>
                            Tamanho: {pokemon.height}
                        </Text>
                    </AppSection>

                    <AppSection
                        title={'Tipos'}>

                        {this.renderPokemonTypes(pokemon.types)}

                    </AppSection>

                    <AppSection
                        title={'Estatisticas'}>

                        <Text>
                            Defesa: {pokemon.defense}
                        </Text>
                        <Text>
                            Ataque: {pokemon.attack}
                        </Text>
                        <Text>
                            Hp: {pokemon.hp}
                        </Text>
                    </AppSection>

                    <AppSection
                        title={'Evoluções'}>
                        <View
                            style={{
                                width: '100%',
                                height: '100%',
                                flex: 1,
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}>
                            {
                                this.renderEvolutions(pokemon.evolutions)
                            }
                        </View>

                    </AppSection>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (store) => ({
    pokemon: store.pokemonDetailsState.pokemon,
    fetching: store.pokemonDetailsState.fetching,
});

const mapDispatchToProps = dispatch => {
    return {
        // dispatching plain actions
        onRequestPokemons: (id) => dispatch({ type: GET_POKEMON_DETAILS_REQUEST, payload: { id: id } }),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailsPage);

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%'
    },
});
