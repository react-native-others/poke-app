import { takeLatest, call, put, all } from "redux-saga/effects";
import {pokemonService} from '../shared/services/PokemonService';

// worker saga: makes the api call when watcher saga sees the action
export function* workerPokemonDetailsSaga(action) {
    try {
        ;
        // const response = yield call(() => {  });
        yield put({ type: "API_CALL_SUCCESS" });

    } catch (error) {
        // dispatch a failure action to the store with the error
        yield put({ type: "API_CALL_FAILURE", error });
    }
}