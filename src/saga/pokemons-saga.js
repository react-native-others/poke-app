import { takeLatest, call, put, all } from "redux-saga/effects";
import {pokemonService} from '../shared/services/PokemonService';

// worker saga: makes the api call when watcher saga sees the action
export function* workerPokemonDetailsSaga(action) {
    try {
        const response = yield call(() => {
            return pokemonService.getPokemonDetails(action.payload.id)
        });
        ;
        yield put({ type: "GET_POKEMON_DETAILS_SUCCESS", pokemon: response });

    } catch (error) {
        // dispatch a failure action to the store with the error
        yield put({ type: "GET_POKEMON_DETAILS_ERROR", error });
    }
}

// worker saga: makes the api call when watcher saga sees the action
export function* workerPokemonsSaga(action) {
    try {
        const response = yield call(pokemonService.getPokemons);
        yield put({ type: "API_CALL_SUCCESS", pokemons: response, call_type: 'pokemons' });

    } catch (error) {
        // dispatch a failure action to the store with the error
        yield put({ type: "API_CALL_FAILURE", error });
    }
}

// worker saga: makes the api call when watcher saga sees the action
export function* workerTypesSaga(action) {
    try {
        const response = yield call(() => {
            return pokemonService.getPokemonSections(action.payload.sections);
        });
        
        yield put({ type: "API_CALL_SUCCESS", sections: response, call_type: 'sections' });
    } catch (error) {
        // dispatch a failure action to the store with the error
        yield put({ type: "API_CALL_FAILURE", error });
    }
}
