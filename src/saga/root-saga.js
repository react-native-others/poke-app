import { takeLatest, all } from "redux-saga/effects";
import {workerPokemonsSaga, workerTypesSaga, workerPokemonDetailsSaga} from '../saga/pokemons-saga';

// watcher saga: watches for actions dispatched to the store, starts worker saga
export function* watcherSaga() {
    yield all([
        takeLatest("GET_POKEMON_DETAILS_REQUEST", workerPokemonDetailsSaga),
        takeLatest("API_CALL_POKEMONS_REQUEST", workerPokemonsSaga),
        takeLatest("API_CALL_TYPES_REQUEST", workerTypesSaga),
    ])
}