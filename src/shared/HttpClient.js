import axios from "axios";

class HttpClient {
    // Create an instance using the config defaults provided by the library
    // At this point the timeout config value is `0` as is the default for the library
    instance;

    constructor() {
        this.instance = axios.create();

        // Add a request interceptor
        this.instance.interceptors.request.use(function (config) {
            ;
            return config;
        }, function (error) {
            return Promise.reject(error);
        });

        // Add a response interceptor
        this.instance.interceptors.response.use(function (response) {
            ;
            return response;
        }, function (error) {
            // Do something with response error
            return Promise.reject(error);
        });
    }

    addToken(token) {
        // Alter defaults after instance has been created
        this.instance.defaults.headers.common['Authorization'] = token;
    }

    get(url, params) {
        return this.instance.get(url, {
            params
        });
    }

    post(url, params) {
        return this.instance.post(url, {
            params
        });
    }
}

const httpClient = new HttpClient();

export default httpClient;