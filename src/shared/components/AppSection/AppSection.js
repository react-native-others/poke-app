import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

export default class AppSection extends Component {
    render() {
        return (
            <View style={[styles.container, this.props.style]}>
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>{this.props.title}</Text>
                </View>
                <View>
                    {this.props.children}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        width: '100%'
    },
    titleContainer: {
        borderBottomColor: '#707070',
        borderBottomWidth: 1,
        marginBottom: 15
    },
    title: {
        fontSize: 20,
        paddingBottom: 5
    }
});