import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image } from 'react-native';

export default class TinyCard extends Component {
    render() {
        const {pokemon} = this.props;
        return (
            <View style={[styles.container, this.props.style]}>
                <View>
                    <Text 
                    style={{textTransform: 'capitalize', textAlign: 'center', fontSize: 14, paddingTop: 10}}>{pokemon.name}</Text>
                </View>
                <View>
                    <Image 
                        source={{
                            uri: pokemon.image
                        }}
                        style={{width: '100%', height: '80%', marginTop: 10}}
                        resizeMode={'contain'}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: 137,
        height: 170,
        backgroundColor: 'white',
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: {
            width: 4,
            height: 4
        },
        shadowRadius: 4,
        shadowOpacity: .5,
        marginTop: 5,
        marginBottom: 12,
        marginLeft: 5,
        marginLeft: 5,
    }
});