import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image } from 'react-native';

export default class BigCarg extends Component {
    render() {
        const {pokemon} = this.props;
        return (
            <View style={[styles.container]}>
                <Image 
                    source={{
                        uri: pokemon.image
                    }}
                    style={{
                        width: '100%', height: 188, marginTop: 10,
                        }}
                    resizeMode={'contain'}
                />

                <Text
                    style={{
                        padding: 10,
                        fontSize: 14,
                        textAlign: "center"
                    }}
                    >{pokemon.name}</Text>

                <View
                    style={{
                        flex: 1,
                        width: '100%',
                        alignItems: 'center',
                        justifyContent: 'center',
                        flexDirection: 'row'
                    }}>
                    <Image 
                        source={require('../../../../assets/card_overlay.png')}
                    />

                    <View
                        style={{
                            flex: 1,
                            width: '100%',
                            height: '100%',
                            alignContent: 'center',
                            justifyContent: 'center',
                            position: 'absolute',
                            bottom: 0,
                            flexDirection: 'row',
                            paddingTop: 6
                        }}>
                        <Image
                            source={require(`../../../../assets/pokeball-inative.png`)}
                            style={{
                                width: 24,
                                height: 24
                            }}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F2F2F2',
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: {
            width: 4,
            height: 4
        },
        shadowRadius: 4,
        shadowOpacity: .5,
        marginBottom: 30,
        marginRight: 12,
        marginLeft: 12,
        marginTop: 6,
    }
});