import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, FlatList } from 'react-native';
import TinyCard from '../TinyCard/TinyCard';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class TypeList extends Component {
    render() {
        const { pokemons, fetching } = this.props;
        return (
            <FlatList
                style={styles.container}
                data={ pokemons }
                renderItem={({item}) => 
                    <TouchableOpacity
                        onPress={() => {
                            if(this.props.onItemClicked) {
                                this.props.onItemClicked(item);
                            }
                        }}>
                        <TinyCard pokemon={item} style={styles.card} />
                    </TouchableOpacity>
                }
                horizontal={true}
                keyExtractor={((item, index) => index.toString())}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%'
    },
    card: {
        marginRight: 15,
    }
});

// shadowColor: '#000000',
//         shadowRadius: 10,
//         shadowOffset: {
//             width: 0,
//             height: 3,
//         },
//         shadowOpacity: .16