class Config {
    basePokemonApiUrl = 'https://pokeapi.co/api/v2/';
    baseAuthApiUrl = 'https://localhost:5001/api/';
}

export const config = new Config();