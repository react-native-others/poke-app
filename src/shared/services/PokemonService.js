import axios from "axios";
import httpClient from '../HttpClient';
import { config } from '../Config';

class PokemonService {
  constructor() { }

  getPokemonSections(sections) {
    return new Promise((resolve, reject) => {
      const promises = [];
      console.log('sections', sections);
      sections.forEach(section => {
        promises.push(httpClient.get(`${config.basePokemonApiUrl}type/${section.name}`, {}));
      });

      Promise.all(promises).then(response => {
        response.forEach(res => {
          const data = res.data;
          sections.find(s => s.name === data.name).pokemons = data.pokemon.map(pk => {
            const p = pk.pokemon;
            return {
              id: this.getId(p.url),
              name: p.name,
              image: this.getImage(p.name)
            };
          });
          resolve(sections);
        });
      }).catch(error => {
        ;
      });
    })
  }

  getDetails(pokemon) {
    return httpClient.get(`${config.basePokemonApiUrl}pokemon/${pokemon}`, {});
  }

  getPokemons() {
    return new Promise((resolve, reject) => {
      httpClient.get(`${config.basePokemonApiUrl}pokemon`, {}).then(response => {
        const data = response.data.results.map(p => {
          return {
            id: p.url.replace(/v\d/, '').replace(/\D/g, ''),
            name: p.name,
            image: `https://img.pokemondb.net/artwork/${p.name}.jpg`
          };
        });
        resolve(data);
      });
    });
  }

  getPokemonDetails(pokemon) {
    return new Promise((resolve, reject) => {
      httpClient.get(`${config.basePokemonApiUrl}pokemon/${pokemon}`).then(response => {
        const data = response.data;
        ;
        const p = {};
        p.id = data.id,
          p.image = this.getImage(data.name);
        p.name = data.name;
        p.weight = data.weight;
        p.height = data.height;
        p.attack = this.filterStats(data.stats, 'attack');
        p.defense = this.filterStats(data.stats, 'defense');
        p.hp = this.filterStats(data.stats, 'hp');
        p.speed = this.filterStats(data.stats, 'speed');
        p.evolutions = [];
        p.types = data.types.map(t => {
          return t.type.name;
        });

        const promises = [
          this.getEvolutionChain(p.name),
          this.getCharacteristic(p.id)
        ];

        Promise.all(promises).then(responses => {
          ;
          const evolutionData = responses[0];
          ;
          if (evolutionData.chain) {
            let actualEvolution = evolutionData.chain.evolves_to[0];
            p.evolutions.push({
              id: this.getId(evolutionData.chain.species.url),
              image: this.getImage(evolutionData.chain.species.name),
              name: evolutionData.chain.species.name
            });
            while (actualEvolution) {
              const actual = actualEvolution;
              p.evolutions.push({
                id: this.getId(actual.species.url),
                image: this.getImage(actual.species.name),
                name: actual.species.name
              });
              actualEvolution = actual.evolves_to[0];
            }
          }

          p.description = responses[1];
          resolve(p);
        });
      });
    });
  }

  getEvolutionChain(species) {
    return new Promise((resolve, reject) => {
      httpClient.get(`${config.basePokemonApiUrl}pokemon-species/${species}`).then(response => {
        httpClient.get(response.data.evolution_chain.url).then(data => {
          resolve(data.data);
        });
      });
    });
  }

  getCharacteristic(id) {
    return new Promise((resolve, reject) => {
      httpClient.get(`${config.basePokemonApiUrl}characteristic/${id}`).then(response => {
        resolve(response.data.descriptions.find(d => d.language.name === 'en').description);
      });
    });
  }

  filterStats(stats, statName) {
    return stats.filter(s => s.stat.name === statName)[0].base_stat || 0;
  }

  getImage(pokemon) {
    
    return `https://img.pokemondb.net/artwork/${pokemon.name || pokemon}.jpg`;
  }

  getId(url) {
    return url.replace(/v\d/, '').replace(/\D/g, '')
  }
}

export const pokemonService = new PokemonService();