import axios from "axios";
import httpClient from '../HttpClient';
import {config} from '../Config';
import {AsyncStorage} from 'react-native';
import { firebaseService } from "../../../Firebase";

const AUTH_STORAGE_NAME = "AUTH";
class AuthService {
    constructor() {}

    isLoggedIn() {
        return new Promise((resolve, reject) => {
            AsyncStorage.getItem(AUTH_STORAGE_NAME).then(auth => {
                resolve(auth !== null && auth !== undefined);
            });
        });
    }

    login(username, password) {
        firebaseService
            .login(username, password)
            .then(response => {
                ;
            })
            .catch(error => {
                ;
            })
    }
}

export const authService = new AuthService();