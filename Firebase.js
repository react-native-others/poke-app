import * as firebase from 'firebase';
import firestore from 'firebase/firestore'

const config = {
    apiKey: "AIzaSyDgSSgS-Kgah7VIJY32NHDVswI3QTNNgps",
    authDomain: "pokeapp-dd88d.firebaseapp.com",
    databaseURL: "https://pokeapp-dd88d.firebaseio.com",
    projectId: "pokeapp-dd88d",
    storageBucket: "pokeapp-dd88d.appspot.com",
    messagingSenderId: "1050447119611",
    appId: "1:1050447119611:web:33f44830c46b34c7"
};
class FirebaseService {
    constructor() {
        firebase.initializeApp(config);
    }

    login(username, password) {
        return new Promise((resolve, reject) => {
            firebase.auth()
                .signInWithEmailAndPassword(username, password)
                .then(loginResponse => {
                    resolve(loginResponse);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    createDoc() {
        firebase.firestore().collection("users").add({
            first: "Ada",
            last: "Lovelace",
            born: 1815
        })
        .then(function(docRef) {
            ;
        })
        .catch(function(error) {
            console.error("Error adding document: ", error);
        });
    }
}

export const firebaseService = new FirebaseService();