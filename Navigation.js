import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';
import HomePage from './src/pages/Home/Home';
import DetailsPage from './src/pages/Details/Details';
import { createStackNavigator, createAppContainer, createBottomTabNavigator } from "react-navigation";
import FavoritesPage from './src/pages/Favorites/Favorites';
import Ionicons from 'react-native-vector-icons/Ionicons';

let HomeStack = createStackNavigator({
  Home: HomePage,
  Details: DetailsPage
});

let FavoritesStack = createStackNavigator({
  Favorites: FavoritesPage
});

let ProfileStack = createStackNavigator({
  Profile: FavoritesPage
});

const HomeIconWithBadge = (props) => {
  // You should pass down the badgeCount in some other ways like react context api, redux, mobx or event emitters.
  // badgeCount={3}
  return <IconWithBadge {...props} />;
}

class IconWithBadge extends React.Component {
  render() {
    const { name, badgeCount, color, size } = this.props;
    return (
      <View style={{ width: 24, height: 24, margin: 5 }}>
        <Ionicons name={name} size={size} color={color} />
        {badgeCount > 0 && (
          <View style={{
            // If you're using react-native < 0.57 overflow outside of the parent
            // will not work on Android, see https://git.io/fhLJ8
            position: 'absolute',
            right: -6,
            top: -3,
            backgroundColor: 'red',
            borderRadius: 6,
            width: 12,
            height: 12,
            justifyContent: 'center',
            alignItems: 'center'
          }}>
            <Text style={{ color: 'white', fontSize: 10, fontWeight: 'bold' }}>{badgeCount}</Text>
          </View>
        )}
      </View>
    );
  }
}

export const TabNavigator = createBottomTabNavigator({
  Home: HomeStack,
  Favorites: FavoritesStack,
  Profile: ProfileStack
},
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        tabBarIcon: ({ focused, horizontal, tintColor }) => {
          const { routeName } = navigation.state;
          let IconComponent = Ionicons;
          let iconName;
          if (routeName === 'Home') {
            iconName = `ios-home`;
            IconComponent = HomeIconWithBadge;
          } else if (routeName === 'Favorites') {
            let imageName = `./assets/pokeball-inative.png`;
            if (focused) {
              return <Image
                style={{ width: 28, height: 28 }}
                source={require('./assets/pokeball.png')}
              />
            } else {
              return <Image
                style={{ width: 28, height: 28 }}
                source={require('./assets/pokeball-inative.png')}
              />
            }

          } else if (routeName === 'Profile') {
            iconName = `ios-person`;
          }

          // You can return any component that you like here!
          return <IconComponent name={iconName} size={25} color={tintColor} />;
        },
      }
    },
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
    },
  });