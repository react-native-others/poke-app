import React, {Component} from 'react';
import { Provider } from 'react-redux';
import { Store } from './src/redux/redux-store';
import { createStackNavigator, createAppContainer, createBottomTabNavigator } from "react-navigation";
import { TabNavigator } from './Navigation';

let Navigation = createAppContainer(TabNavigator);
export default class App extends Component {
  render() {
    return (
      <Provider store={Store}>
        <Navigation />
      </Provider>
    );
  }
}